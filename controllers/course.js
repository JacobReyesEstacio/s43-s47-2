const Course = require("../models/Course");

// ACTIVITY: Add Course Controller
module.exports.addCourse = (req, res) => {
  const newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });

  return newCourse
    .save()
    .then((course, error) => {
      if (error) {
        return res.send(false);
      } else {
        console.error(newCourse);
        return res.send(true);
      }
    })
    .catch((err) => res.send(err));
};




module.exports.getAllCourses = (req,res) => {
  return Course.find({}).then(result=> {
    return res.send(result)
  })
}

module.exports.getAllActiveCourses = (req,res) => {
  return Course.find({isActive:true}).then(result=> {
    return res.send(result)
  })
}

module.exports.getCourse = (req,res) => {
  return Course.findById(req.params.courseId).then(result=> {
    return res.send(result)
  })
}

module.exports.updateCourse = (req,res) => {

    let updatedCourse = {
      name : req.body.name,
      description : req.body.description,
      price : req.body.price
    }

    return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course,error)=> {
      if (error) {
        return res.send(false);
      }else {
        res.send(true);
      }
    })

}


module.exports.archiveCourse = (req,res)=>{
  return Course.findByIdAndUpdate(req.params.courseId, {isActive: false}).then(course=>{
          if(course){
              return res.send(true);
          }else{
              return res.send(false);
          }
  })
  .catch(error => {
          return res.send(false);
      });
}


module.exports.activateCourse = (req, res) => {
  const courseId = req.params.courseId;

  return Course.findByIdAndUpdate(courseId, { isActive: true }).then(
    (course, error) => {
      if (error) {
        return res.send(false);
      } else {
        return res.send(true);
      }
    }
  );
};





