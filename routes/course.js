// Dependencies
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// ACTIVITY: Admin Course Creation
router.post("/", verify, verifyAdmin, courseController.addCourse);

router.get("/all", courseController.getAllCourses);

router.get("/", courseController.getAllActiveCourses);

router.get("/:courseId", courseController.getCourse);

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate",verify, verifyAdmin, courseController.activateCourse);
// [Export Route System]
module.exports = router;

