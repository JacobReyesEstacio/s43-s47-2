// Create a simple Express JS Application

// Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// Cors allows the backend application to be available to our frontend application

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course")

// Environment Setup
const port = 4000;

// Server Setup
const app = express();
// Creates a variable that stores the express function, which initializes the application and allows us to have access to the different methods that create the backend

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Database Connection
// Connect to MongoDB Database
mongoose.connect(
  "mongodb+srv://Jacob:flr88ax45bvg@cluster0.q6y4s9e.mongodb.net/S43-S47?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Prompt
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas."));

// [Backend Routes]
// http://localhost:4000/users
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
// Server Gateway Response
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on Port ${process.env.PORT || port}`);
  });
}

module.exports = { app, mongoose };

